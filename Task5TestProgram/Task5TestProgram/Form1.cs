﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestImageOperators;
using InterfaceImageOperators;
using OpenCvSharp;
using System.Linq.Expressions;
using OpenCvSharp.Extensions;
using System.Reflection;
using ClassTask5RamonGiron;
namespace Task5TestProgram
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        List<Func<Mat, TestResults>> functions = new List<Func<Mat, TestResults>>();
        Mat img;
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i = (int)comboBox1.SelectedIndex;
            TestResults results = functions[i].Invoke(img);
            OriginalPictureBox.Image = BitmapConverter.ToBitmap(img);
            OpenCVPictureBox.Image = BitmapConverter.ToBitmap(results.CvMat);
            OwnPictureBox.Image = BitmapConverter.ToBitmap(results.OwnMat);
            DifferencesPictureBox.Image = BitmapConverter.ToBitmap(results.DiffMat);
            textBox1.Text = results.EqualImages.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            img = Cv2.ImRead("../../images/smallimage.png");
            IImageOperators operators = new ImageOperatorsclass();
            TestFunctions testFunctions = new TestFunctions(operators);
            foreach (var function in typeof(TestFunctions).GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly))
            {
                var func = Delegate.CreateDelegate(typeof(Func<Mat, TestResults>), testFunctions, function, false);
                functions.Add((Func<Mat, TestResults>)func);
                comboBox1.Items.Add(function.Name);
            }
        }
    }
}
